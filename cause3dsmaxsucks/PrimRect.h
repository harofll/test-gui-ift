#pragma once
#include "PrimitiveVectorielle.h"
#include "ofMain.h"

class PrimRect : protected PrimitiveVectorielle
{
public:
	PrimRect();
	PrimRect(float width, float height);

	~PrimRect();

	void setWidth(float width);
	float getWidth();

	void setHeight(float height);
	float getHeight();

	//void setRect();
	//void getRect();

	//ofRectangle createSquare(float posX, float posY,  float width, float height);
	// Qu'es que cette fonction retourne ... ? void ?

private:
	float m_width;
	float m_height;
	//ofRectangle m_rect;
};
