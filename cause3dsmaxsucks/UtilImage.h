#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofPath.h"

class UtilImage
{

public:
	void Import();
	void MaskImage();
	void Export();
	void SetColor(float r, float g, float b, float a);

	ofImage importImage;
	ofFbo fbo;
	ofColor color;
};
