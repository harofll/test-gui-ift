#pragma once

#include "ofMain.h"
#include "PrimitiveVectorielle.h"

class PrimitiveVectorielle
{
public:
	PrimitiveVectorielle();
	PrimitiveVectorielle(float positionX, float positionY);
	virtual ~PrimitiveVectorielle();
	
	void setPositionX(float positionX);
	float getPositionX();

	void setPositionY(float positionY);
	float getPositionY();


protected:
	float m_posX;
	float m_posY;




};