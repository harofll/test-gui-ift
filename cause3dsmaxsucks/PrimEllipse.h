#pragma once
#include "PrimitiveVectorielle.h"
#include "ofMain.h"

class PrimEllipse : protected PrimitiveVectorielle
{
public:
	PrimEllipse();
	PrimEllipse(float width, float height);

	~PrimEllipse();

	void setWidth(float width);
	float getWidth();

	void setHeight(float height);
	float getHeight();



private:
	float m_width;
	float m_height;
	

};

