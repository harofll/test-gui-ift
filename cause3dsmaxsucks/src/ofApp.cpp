#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
	this->gui.setup();
	this->gui.add(radius.setup("radius", 50.0, 0.0, 100.0));
	utilImage = UtilImage();
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
	ofSetColor(0, 0, 0, 255);
	ofDrawCircle(ofGetWidth() / 2, ofGetHeight() / 2, this->radius);
	this->gui.draw();

	ofEnableAlphaBlending();
	ofSetColor(utilImage.color);
	//if (importImage.isUsingTexture())
	utilImage.importImage.draw(0, 0);
	ofDisableAlphaBlending();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == OF_KEY_F1)
		utilImage.Import();
	else if (key == OF_KEY_F3)
		utilImage.SetColor(255, 0, 0, 255); //teindre avec valeurs des sliders
	else if (key == OF_KEY_F4)
		utilImage.Export();
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
