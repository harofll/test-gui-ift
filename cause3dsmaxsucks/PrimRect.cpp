#include "PrimRect.h"



PrimRect::PrimRect() : PrimitiveVectorielle(m_posX, m_posY), m_width(0.0), m_height(0.0)
{
}

PrimRect::PrimRect (float width, float height) :
PrimitiveVectorielle(m_posX, m_posY)
{
	this->m_width = width;
	this->m_height = height;
}

PrimRect::~PrimRect()
{
}


void PrimRect::setWidth(float width)
{
	m_width = width;
}

float PrimRect::getWidth()
{
	return m_width;
}

void PrimRect::setHeight(float height)
{
	m_height = height;
}

float PrimRect::getHeight()
{
	return m_height;
}
