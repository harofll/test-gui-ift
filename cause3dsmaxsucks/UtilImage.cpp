#include "UtilImage.h"

void UtilImage::Import()
{
	ofFileDialogResult result = ofSystemLoadDialog("Load file");
	if (result.bSuccess)
	{
		string path = result.getPath();
		importImage.load(path);
	}
}

void UtilImage::MaskImage()
{
	ofPath p = ofPath();

	ofPoint points[4];
	
	for (int i = 0; i < 4; i++)
	{
		p.lineTo(points[i]);
	}

	fbo.allocate(importImage.getWidth(), importImage.getHeight(), GL_RGBA);
	fbo.begin();
	ofSetColor(100);
	ofDrawRectangle(0, 0, importImage.getWidth(), importImage.getHeight());
	fbo.end();

	importImage.getTexture().setAlphaMask(fbo.getTexture());
}

void UtilImage::Export()
{
	ofPixels pixels;
	fbo.allocate(importImage.getWidth(), importImage.getHeight(), GL_RGBA);
	fbo.begin();
	importImage.draw(0, 0);
	fbo.end();

	fbo.readToPixels(pixels);
  
	ofSaveImage(pixels, "image.jpg", OF_IMAGE_QUALITY_BEST);
}

void UtilImage::SetColor(float r, float g, float b, float a)
{
	color.r = r;
	color.g = g;
	color.b = b;
	color.a = a;
}
