#include "PrimTriangle.h"



PrimTriangle::PrimTriangle() : PrimitiveVectorielle(m_posX, m_posY),
m_posX_2(0.0), m_posY_2(0.0), m_posX_3(0.0), m_posY_3(0.0)
{
}

PrimTriangle::PrimTriangle(float posX_2, float posY_2, float posX_3, float posY_3) :
PrimitiveVectorielle (m_posX, m_posY)
{
	this->m_posX_2 = posX_2;
	this->m_posY_2 = posY_2;
	this->m_posX_3 = posX_3;
	this->m_posY_3 = posY_3;
}

PrimTriangle::~PrimTriangle()
{
}


void PrimTriangle::setPosX2(float posX_2)
{
	m_posX_2 = posX_2;
}

float PrimTriangle::getPosX2()
{
	return m_posX_2;
}

void PrimTriangle::setPosY2(float posY_2)
{
	m_posY_2 = posY_2;
}

float PrimTriangle::getPosY2()
{
	return m_posY_2;
}

void PrimTriangle::setPosX3(float posX_3)
{
	m_posX_3 = posX_3;
}

float PrimTriangle::getPosX3()
{
	return m_posX_3;
}

void PrimTriangle::setPosY3(float posY_3)
{
	m_posY_3 = posY_3;
}

float PrimTriangle::getPosY3()
{
	return m_posY_3;
}
