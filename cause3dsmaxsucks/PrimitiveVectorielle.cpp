#include "PrimitiveVectorielle.h"



PrimitiveVectorielle::PrimitiveVectorielle() : m_posX(0.0), m_posY(0.0)
{
}

PrimitiveVectorielle::PrimitiveVectorielle (float positionX, float positionY) : m_posX(positionX), m_posY(positionY)
{
}

PrimitiveVectorielle::~PrimitiveVectorielle()
{
}


void PrimitiveVectorielle::setPositionX(float positionX)
{
	m_posX = positionX;
}

float PrimitiveVectorielle::getPositionX()
{
	return m_posX;
}

void PrimitiveVectorielle::setPositionY(float positionY)
{
	m_posY = positionY;
}

float PrimitiveVectorielle::getPositionY()
{
	return m_posY;
}


