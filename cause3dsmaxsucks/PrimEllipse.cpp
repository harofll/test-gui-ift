#include "PrimEllipse.h"



PrimEllipse::PrimEllipse() : PrimitiveVectorielle(m_posX, m_posY), m_width(0.0), m_height(0.0)
{
}

PrimEllipse::PrimEllipse(float width, float height) :
PrimitiveVectorielle(m_posX, m_posY)
{
	this->m_width = width;
	this->m_height = height;
}

PrimEllipse::~PrimEllipse()
{
}


void PrimEllipse::setWidth(float width)
{
	m_width = width; 
}

float PrimEllipse::getWidth()
{
	return m_width;
}

void PrimEllipse::setHeight(float height)
{
	m_height = height;
}

float PrimEllipse::getHeight()
{
	return m_height;
}
