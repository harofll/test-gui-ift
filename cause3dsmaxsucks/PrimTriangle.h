#pragma once
#include "PrimitiveVectorielle.h"
#include "ofMain.h"

class PrimTriangle : protected PrimitiveVectorielle
{
public:
	PrimTriangle();
	PrimTriangle(float posX_2, float posY_2, float posX_3, float posY_3);
	~PrimTriangle();

	void setPosX2(float posX_2);
	float getPosX2();

	void setPosY2(float posY_2);
	float getPosY2();

	void setPosX3(float posX_3);
	float getPosX3();

	void setPosY3(float posY_3);
	float getPosY3();



private:
	float m_posX_2;
	float m_posY_2;
	float m_posX_3;
	float m_posY_3;
};

